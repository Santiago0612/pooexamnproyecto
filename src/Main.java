// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List<Empleado> empleados = new ArrayList<>();
        List<Proyecto> proyectos = new ArrayList<>();
        List<Asignacion> asignaciones = new ArrayList<>();

        empleados.add(new Empleado(1, "Alice", "Brown", 50000));
        empleados.add(new Empleado(2, "Bob", "Smith", 60000));

        proyectos.add(new Proyecto("Proyecto A", "2022-01-01", "2022-06-01"));
        proyectos.add(new Proyecto("Proyecto B", "2022-02-01", "2022-12-01"));

        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Sistema de gestión de empleados y proyectos");
            System.out.println("1. Listar empleados");
            System.out.println("2. Listar proyectos");
            System.out.println("3. Asignar empleado a proyecto");
            System.out.println("4. Listar asignaciones");
            System.out.println("5. Salir");

            int opcion = scanner.nextInt();
            scanner.nextLine();  // Limpiar el buffer

            switch (opcion) {
                case 1:
                    for (Empleado empleado : empleados) {
                        System.out.println(empleado.getNombre());
                    }
                    break;
                case 2:
                    for (Proyecto proyecto : proyectos) {
                        System.out.println(proyecto.getNombre());
                    }
                    break;
                case 3:
                    System.out.println("Ingrese el ID del empleado:");
                    int idEmpleado = scanner.nextInt();
                    System.out.println("Ingrese el nombre del proyecto:");
                    scanner.nextLine();  // Limpiar el buffer
                    String nombreProyecto = scanner.nextLine();

                    Empleado empleadoSeleccionado = null;
                    Proyecto proyectoSeleccionado = null;

                    for (Empleado empleado : empleados) {
                        if (empleado.getId() == idEmpleado) {
                            empleadoSeleccionado = empleado;
                            break;
                        }
                    }

                    for (Proyecto proyecto : proyectos) {
                        if (proyecto.getNombre().equals(nombreProyecto)) {
                            proyectoSeleccionado = proyecto;
                            break;
                        }
                    }

                    if (empleadoSeleccionado != null && proyectoSeleccionado != null) {
                        System.out.println("Ingrese las horas asignadas:");
                        int horas = scanner.nextInt();
                        Asignacion nuevaAsignacion = new Asignacion(empleadoSeleccionado, proyectoSeleccionado, horas);
                        asignaciones.add(nuevaAsignacion);
                        System.out.println("Asignación exitosa.");
                    } else {
                        System.out.println("Empleado o proyecto no encontrado.");
                    }

                    break;
                case 4:
                    for (Asignacion asignacion : asignaciones) {
                        System.out.println(asignacion);
                    }
                    break;
                case 5:
                    System.out.println("Saliendo...");
                    return;
                default:
                    System.out.println("Opción no válida.");
            }
        }
    }
}