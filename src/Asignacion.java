public class Asignacion {
    private Empleado empleado;
    private Proyecto proyecto;
    private int horasAsignadas;

    public Asignacion(Empleado empleado, Proyecto proyecto, int horasAsignadas) {
        this.empleado = empleado;
        this.proyecto = proyecto;
        this.horasAsignadas = horasAsignadas;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public Proyecto getProyecto() {
        return proyecto;
    }

    public void setProyecto(Proyecto proyecto) {
        this.proyecto = proyecto;
    }

    public int getHorasAsignadas() {
        return horasAsignadas;
    }

    public void setHorasAsignadas(int horasAsignadas) {
        this.horasAsignadas = horasAsignadas;
    }

    @Override
    public String toString() {
        return "Asignacion{" +
                "empleado=" + empleado.getNombre() +
                ", proyecto=" + proyecto.getNombre() +
                ", horasAsignadas=" + horasAsignadas +
                '}';
    }
}
