public class Empleado {
    private int id;
    private String nombre;
    private String apellido;
    private int salrario;

    public Empleado(int id, String nombre, String apellido, int salrario) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.salrario = salrario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getSalrario() {
        return salrario;
    }

    public void setSalrario(int salrario) {
        this.salrario = salrario;
    }
}
